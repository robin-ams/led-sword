#include <FastLED.h>

#define LED_PIN     1
#define NUM_LEDS    8
#define BRIGHTNESS  255
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
CRGB leds[NUM_LEDS];

#define BUTTON_PIN 0
#define EXTENDER_POT_PIN 2
#define TUNE_POT_PIN 3

// number of pot readings to get an avarage for smooth reault
#define SMOOTH_READINGS 15

bool lastButtonState = LOW;
bool buttonPressed = false;

uint8_t buttonState;
uint8_t mode = 0;
uint8_t potOneVal = 0;
uint8_t ledsOn = 0;

unsigned long lastDebounceTime;

CRGBPalette16 currentPalette;
TBlendType    currentBlending;

void setup() {

    delay( 3000 ); // power-up safety delay

    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>( leds, NUM_LEDS ).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );

    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;

    pinMode( BUTTON_PIN, INPUT );
}

void loop() {

    if ( mode > 8 ) {
        mode = 0;
    }

    if (mode == 0 ) {
        hsvColor();
    }
    else if ( mode == 1 ) {
        currentPalette = PartyColors_p;
    }
    else if ( mode == 2 ) {
        currentPalette = LavaColors_p;
    }
    else if ( mode == 3 ) {
        currentPalette = CloudColors_p;
    }
    else if (mode == 4) {
        currentPalette = HeatColors_p;
    }
    else if (mode == 5) {
        currentPalette = ForestColors_p;
    }
    else if (mode == 6) {
        currentPalette = OceanColors_p;
    }
    else if (mode == 7) {
        currentPalette = RainbowColors_p;
    }
    else if (mode == 8) {
        currentPalette = RainbowStripeColors_p;
    }

    if ( mode > 0 ) {

        static uint8_t startIndex = 0;
        startIndex = startIndex + 1; /* motion speed */

        FillLEDsFromPaletteColors( startIndex );
    }

    FastLED.show();

    int delay = 0;

    if ( mode == 0 ) {
        delay = 0;
    }
    else {
        delay = potOneVal;
    }

    for (size_t i = 0; i < (delay/30)+1; i++) {
        readButton();
        ledExtender();
        potOne();
    }
}

void hsvColor () {

    for( int i = ledsOn ; i < NUM_LEDS; i++ ) {
        leds[i] = CRGB( 0, 0, 0);
    }
    for( int i = 0; i <= ledsOn; i++ ) {
        leds[i] = CHSV( potOneVal, 255,255);
    }
}

void potOne () {
    int reading = 0;

    // smoothing the pot reading
    for (size_t i = 0; i < SMOOTH_READINGS; i++) {
        reading += analogRead( TUNE_POT_PIN );
    }
    reading = reading / SMOOTH_READINGS;
    delay(1);
    potOneVal = map(reading , 0,1023, 0, 255);
}

void ledExtender () {

    int readExtender = 0;

    // smoothing the pot reading
    for (size_t i = 0; i < SMOOTH_READINGS; i++) {
        readExtender += analogRead( EXTENDER_POT_PIN );
    }
    readExtender = readExtender / SMOOTH_READINGS;
    delay(1);

    ledsOn = map(readExtender , 0,1023, 0, NUM_LEDS);
}

bool readButton () {

    int reading = digitalRead( BUTTON_PIN );

    if (reading != lastButtonState) {
        lastDebounceTime = millis();
    }
    if ((millis() - lastDebounceTime) > 20) { // debounce delay 20

        if (reading != buttonState) {
            buttonState = reading;

            if (buttonState == HIGH) {
                buttonPressed = true;
                return false;
            }

            if (buttonState == LOW && buttonPressed) {

                mode++;
                buttonPressed = false;
                return true;
            }
        }
    }
    lastButtonState = reading;
}

void FillLEDsFromPaletteColors( uint8_t colorIndex) {

    for(int i = ledsOn ; i < NUM_LEDS; i++ ) {
        leds[i] = CRGB( 0, 0, 0);
    }
    for( int i = 0; i <= ledsOn; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, BRIGHTNESS, currentBlending);
        colorIndex -= 6;
    }
}
